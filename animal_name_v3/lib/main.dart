import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import './pages/home_page.dart';
import './pages/splash_screen_page.dart';
import './pages/animal_page.dart';

void main() {
  SystemChrome.setPreferredOrientations([
    DeviceOrientation.portraitUp,
    DeviceOrientation.portraitDown
  ]);

  SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.light);

  runApp(new AnimalApp());
}

class AnimalApp extends StatelessWidget {
  final Map<String, WidgetBuilder> _routes = {
    '/home': (BuildContext context) => HomePage(),
    '/splash-screen': (BuildContext context) => SplashScreenPage(),
    '/animal': (BuildContext context) => AnimalPage(),
  };

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {

    return MaterialApp(
      title: "Nama Hewan",
      debugShowCheckedModeBanner: false,
      theme: new ThemeData(
          primarySwatch: Colors.purple,
          primaryColor: Colors.blue, //dependency with dialog datetime
          primaryColorDark: Colors.black,
          primaryIconTheme: IconThemeData(color: Colors.white),
          hintColor: Colors.white,

          fontFamily: 'Helvetica Neue',
          dialogBackgroundColor: Colors.transparent,
          //backgroundColor: kBMSOriginal,
          //textSelectionColor: kTelkomselSteelGrey.withOpacity(0.5),
          //textSelectionHandleColor: Colors.white,

          //primaryColorBrightness: Brightness.dark,
          //textTheme: TextTheme(body1: TextStyle(color: Colors.white),caption: TextStyle(color: kTelkomselSteelGrey), subhead: TextStyle(color: Colors.white), ),
          primaryTextTheme: TextTheme(subhead: TextStyle(color: Colors.white),),

          unselectedWidgetColor: Colors.white,
          scaffoldBackgroundColor: Colors.white
      ),
      home: SplashScreenPage(), //statusAgreement ? IntroPage() : MenuBankPage(),//new LoginPage(),
      routes: _routes,
    );
  }
}
