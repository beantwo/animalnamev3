import 'dart:core';
import '../objects/nav_object.dart';

class NavService {
  static const ID = 'NavService';

  static final NavService _instance = new NavService();

  static NavService get() => _instance;
  NavObject navObject = new NavObject();

  NavObject navObj() => navObject;
}