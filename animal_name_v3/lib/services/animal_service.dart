import 'dart:core';
import 'dart:convert';
import 'package:flutter/services.dart' show rootBundle;
import '../objects/animal_object.dart';
import '../objects/animal_group_object.dart';

class AnimalService {
  static const ID = 'AnimalService';
  static final AnimalService _instance = new AnimalService();

  static AnimalService get() => _instance;
  List<Animal> animals = new List<Animal>();

  List<Animal> an() => animals;
  String jsonString;

  Future<List<Animal>> generateAnimalGroups() async {
    jsonString = await rootBundle.loadString('assets/json/animals.json');
    List<dynamic> animalList = jsonDecode(jsonString);
    for(var i = 0; i < animalList.length; i++) {
      Animal animal = Animal.fromJson(animalList[i]);
      animals.add(animal);
    }
    return animals;
  }

  Future<List<Animal>> getAnimalsByTag(String tag) async {
    animals = await generateAnimalGroups();
    List<Animal> animalTagged = animals.where((a) => a.tag == tag).toList();
    return animalTagged;
  }

}