import 'animal_object.dart';

class AnimalGroup {
  final String tag;
  final List<Animal> animals;

  AnimalGroup(this.tag, this.animals);
}