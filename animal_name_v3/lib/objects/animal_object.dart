class Animal {
  final String id;
  final String name;
  final String title;
  final String description;
  final String tag;
  final String habitat;
  final String makanan;
  final String status;
  final String image;

  Animal(this.id, this.name, this.title, this.description, this.tag, this.habitat, this.makanan, this.status, this.image);

  Animal.fromJson(Map<String, dynamic> json)
      : id = json['_id'],
        name = json['name'],
        title = json['title'],
        description = json['description'],
        tag = json['tag'],
        habitat = json['habitat'],
        makanan = json['makanan'],
        status = json['status'],
        image = json['image'];
//
//  Map<String, dynamic> toJson() =>
//      {
//        'name': name,
//        'title': title,
//      };
}