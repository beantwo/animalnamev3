import 'package:flutter/material.dart';
import '../services/nav_service.dart';
import '../objects/nav_object.dart';
import '../utils/param_constant.dart';

class HomePage extends StatefulWidget {
  @override
  HomePageState createState() => HomePageState();
}

class HomePageState extends State<HomePage> {
  static const ID = 'HomePageState';

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
          backgroundColor: anPeach,
          body: _displayBody(context),
        ));
  }

  Widget _displayBody(BuildContext context) {
    return Container(
        decoration: const BoxDecoration(
          color: anPeach,
        ),
        child: ListView(
          children: <Widget>[
            _displayTitle(context),
            _displayContent(context),
          ],
        )
    );
  }

  Widget _displayTitle(BuildContext context) {
    return Container(
        margin: EdgeInsets.only(top: 40.0, left: 30.0, bottom: 10.0, right: 30.0),
        width: double.infinity,
        height: 150,
        child: Column(
          children: <Widget>[
            Center(
              child: Image.asset('assets/identities/appName.png',)
            ),
            Container(
              width: double.maxFinite,
              margin: EdgeInsets.fromLTRB(150, 20, 30, 10),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.end,
                children: <Widget>[
                  Icon(Icons.star, size: 20, color: Colors.red,),
                  Icon(Icons.star, size: 25, color: Colors.green,),
                  Icon(Icons.star, size: 20, color: Colors.blue,)
                ],
              ),
            )
          ],
        )
    );
  }

  Widget _displayContent(BuildContext context) {
    List<String> alphabets = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K",
    "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"];
    var widgets = alphabets.map((alp) {
      return FlatButton(
        child: Container(
          decoration: BoxDecoration(color: Colors.white,
          border: Border.all(color: Colors.black, width: 2.0),
          borderRadius: BorderRadius.circular(12.0),
          boxShadow: [ BoxShadow(color: Colors.grey, offset: Offset(0, 5), blurRadius: 5.0) ]),
          child: Image.asset('assets/alphabets/icon_' + alp + '.png', width: 128,),
        ),
        onPressed: () {
//          NavService().updateAlphabet(alp);
        NavObject navObject = NavService.get().navObj();
        navObject.alphabet = alp;
          Navigator.pushNamed(context, '/animal');
        },
      );
    }).toList();
    return Container(
      margin: EdgeInsets.only(left: 30.0, right: 30.0),
      child: GridView.count(
        shrinkWrap: true,
          crossAxisCount: 2,
        children: widgets,
      ),
    );
  }
}