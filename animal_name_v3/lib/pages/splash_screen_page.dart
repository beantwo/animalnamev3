import 'dart:async';

import 'package:flutter/material.dart';
import 'package:bloc/bloc.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../bloc/init_bloc.dart';
import '../bloc/logging_bloc_delegate.dart';
import '../services/animal_service.dart';
import '../utils/param_constant.dart';

class SplashScreenPage extends StatefulWidget {
  @override
  SplashScreenPageState createState() => SplashScreenPageState();
}

class SplashScreenPageState extends State<SplashScreenPage> {
  InitBloc _initBloc;

  @override
  void initState() {
    super.initState();
    _initBloc = InitBloc();
    _initBloc.dispatch(0);

    BlocSupervisor().delegate = LoggingBlocDelegate();
  }

  @override
  void dispose() {
    _initBloc?.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
          backgroundColor: anPeach,
          body: _displayLoading(context),
        ));
  }

  Widget _displayLoading(BuildContext context) {
    return BlocBuilder<int, InitState>(
      bloc: _initBloc,
      builder: (context, InitState initState) {
        print(initState);
        if (initState.isError) {
          return Text("Oops! Ada yang salah!",
            style: TextStyle(color: Colors.black, fontSize: 12.0, fontWeight: FontWeight.bold),
          );
        } else if (initState.isFinished()) {
          Future.delayed(Duration(milliseconds: 10), () {
            Navigator.pushReplacementNamed(context, '/home');
          });
          return Text(initState.currentValue.toString(),
              style: TextStyle(color: Colors.black, fontSize: 12.0, fontWeight: FontWeight.bold));
        } else {
          return Container(
            margin: EdgeInsets.fromLTRB(200, 300, 150, 100),
            child: CircularProgressIndicator(),
          );
        }
      },
    );
  }
}