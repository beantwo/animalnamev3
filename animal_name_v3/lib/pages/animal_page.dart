import 'package:flutter/material.dart';
import 'package:flutter/services.dart' show rootBundle;
import 'package:carousel_slider/carousel_slider.dart';
import 'dart:async';

import 'package:flutter/material.dart';
import 'package:bloc/bloc.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:audioplayers/audio_cache.dart';
import 'package:audioplayers/audioplayers.dart';

import '../bloc/animal_bloc.dart';

import '../services/nav_service.dart';
import '../services/animal_service.dart';
import '../objects/nav_object.dart';
import '../objects/animal_object.dart';
import '../utils/param_constant.dart';

class AnimalPage extends StatefulWidget {
  @override
  AnimalPageState createState() => AnimalPageState();
}

class AnimalPageState extends State<AnimalPage> {
  static const ID = 'AnimalPageState';

  NavObject navObj = NavService.get().navObj();
  List<Animal> animals = new List<Animal>();

  AudioPlayer audioPlayer = new AudioPlayer();
  AudioCache audioCache = new AudioCache(prefix: 'sounds/');

  AnimalBloc _animalBloc;
  var ratio = 64.0;
  var iconSize = 36.0;

  @override
  void initState() {
    super.initState();
    audioCache.fixedPlayer = audioPlayer;
    _animalBloc = AnimalBloc();
    _animalBloc.dispatch(0);
  }

  @override
  void dispose() {
    _animalBloc?.dispose();
    super.dispose();
  }

  playAudio(String localPath) async {
    audioPlayer.stop();
    audioCache.play(localPath);
  }

  @override
  Widget build(BuildContext context) {
    var height = MediaQuery.of(context).size.height;
    var width = MediaQuery.of(context).size.width;
    iconSize = width / height * ratio;
    return WillPopScope(
        onWillPop: () => _exitApp(context),
        child: Scaffold(
      backgroundColor: anPeach,
      appBar: AppBar(
        iconTheme: IconThemeData(color: Colors.black),
        title: Text(
          "Hewan dari huruf " + navObj.alphabet,
          style: TextStyle(color: Colors.black),
        ),
        backgroundColor: anCream,
      ),
      body: _displayBody(context),
    ));
  }

  Future<bool> _exitApp(BuildContext context) {
    audioPlayer.stop();
    return new Future<bool>.value(true);
  }

  Future myLoadAsset(String path) async {
    try {
      var data = await rootBundle.loadString(path);
    } catch(_) {
      return null;
    }
  }

  Widget _displayBody(BuildContext context) {
    return BlocBuilder<int, AnimalState>(
      bloc: _animalBloc,
      builder: (context, AnimalState animalState) {
        animalState.tag = navObj.alphabet;
        if (animalState.isError) {
          return Text(
            "Oops! Ada yang salah!",
            style: TextStyle(
                color: Colors.black,
                fontSize: 12.0,
                fontWeight: FontWeight.bold),
          );
        } else if (animalState.isFinished) {
          if (animalState.length == 0) {
            return Text("No Data Found");
          } else {
            return Container(
                child: Container(
              margin: EdgeInsets.only(top: 50.0),
              child: CarouselSlider(
                onPageChanged: (index) {
                  audioPlayer.stop();
                },
                height: MediaQuery.of(context).size.height * .7,
                items: animalState.animals.map((animal) {
                  return Builder(
                    builder: (BuildContext context) {
                      return Container(
                          width: MediaQuery.of(context).size.width,
                          margin: EdgeInsets.symmetric(horizontal: 15.0),
                          decoration: BoxDecoration(
                              color: anCream,
                              borderRadius: BorderRadius.circular(12.0),
                              border:
                                  Border.all(color: Colors.black, width: 2.0),
                              boxShadow: [
                                BoxShadow(
                                    color: Colors.grey,
                                    offset: Offset(0, 5),
                                    blurRadius: 5.0)
                              ]),
                          child: ClipRRect(
                              borderRadius: BorderRadius.only(
                                topLeft: Radius.circular(10.0),
                                topRight: Radius.circular(10.0),
                              ),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Container(
                                    height:
                                        MediaQuery.of(context).size.height * .4,
                                    width: double.maxFinite,
                                    child: Image.asset(
                                      'assets/images/' + animal.image,
                                      fit: BoxFit.cover,
                                      alignment: Alignment.center,
                                    ),
                                  ),
                                  Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: <Widget>[
                                      Container(
                                        margin: EdgeInsets.only(
                                            top: 10.0, left: 20.0),
                                        child: Text(
                                          animal.name,
                                          textAlign: TextAlign.left,
                                          style: TextStyle(
                                              color: Colors.purple,
                                              fontWeight: FontWeight.bold,
                                              fontSize: 18.0),
                                        ),
                                      ),
                                      FlatButton(
                                              onPressed: () {
                                                playAudio(animal.name + '.mp3');
                                              },
                                              child: Row(
                                                children: <Widget>[
                                                  Text(
                                                    'Suara',
                                                    textAlign: TextAlign.left,
                                                    style: TextStyle(
                                                        color: Colors.blue,
                                                        fontWeight:
                                                            FontWeight.bold,
                                                        fontSize: 16.0),
                                                  ),
                                                  Icon(
                                                    Icons.audiotrack,
                                                    color: Colors.blue,
                                                    size: iconSize / 2,
                                                  ),
                                                ],
                                              ))
                                    ],
                                  ),
                                  Container(
                                    margin:
                                        EdgeInsets.only(top: 10.0, left: 20.0),
                                    child: Text(
                                      animal.title,
                                      textAlign: TextAlign.left,
                                      style: TextStyle(
                                          color: Colors.orange,
                                          fontWeight: FontWeight.bold,
                                          fontSize: 14.0),
                                    ),
                                  ),
                                  Container(
                                    margin: EdgeInsets.only(
                                        top: 10.0,
                                        left: 20.0,
                                        right: 20.0,
                                        bottom: 10.0),
                                    child: Divider(
                                      height: 15,
                                      color: Colors.black,
                                    ),
                                  ),
                                  Container(
                                    child: Row(
                                      children: <Widget>[
                                        Expanded(
                                          child: FlatButton(
                                            child: Column(
                                              children: <Widget>[
                                                Container(
                                                  child: Image.asset(
                                                    'assets/icons/food.png',
                                                    width: iconSize,
                                                  ),
                                                ),
                                                Container(
                                                  child: Text(
                                                    'Makanan',
                                                    style: TextStyle(
                                                        color: Colors.red,
                                                        fontSize: 12.0),
                                                  ),
                                                )
                                              ],
                                            ),
                                            onPressed: () {
                                              _showDialog(context, 'Makanan',
                                                  animal.makanan, Colors.red);
                                            },
                                          ),
                                        ),
                                        Expanded(
                                          child: FlatButton(
                                            child: Column(
                                              children: <Widget>[
                                                Container(
                                                  child: Image.asset(
                                                    'assets/icons/habitat.png',
                                                    width: iconSize,
                                                  ),
                                                ),
                                                Container(
                                                  child: Text(
                                                    'Habitat',
                                                    style: TextStyle(
                                                        color: Colors.green,
                                                        fontSize: 12.0),
                                                  ),
                                                )
                                              ],
                                            ),
                                            onPressed: () {
                                              _showDialog(context, 'Habitat',
                                                  animal.habitat, Colors.green);
                                            },
                                          ),
                                        ),
                                        Expanded(
                                          child: FlatButton(
                                            child: Column(
                                              children: <Widget>[
                                                Container(
                                                  child: Image.asset(
                                                    'assets/icons/deskripsi.png',
                                                    width: iconSize,
                                                  ),
                                                ),
                                                Container(
                                                  child: Text(
                                                    'Catatan',
                                                    style: TextStyle(
                                                        color: Colors.purple,
                                                        fontSize: 12.0),
                                                  ),
                                                )
                                              ],
                                            ),
                                            onPressed: () {
                                              _showDialog(
                                                  context,
                                                  'Catatan',
                                                  animal.description,
                                                  Colors.purple);
                                            },
                                          ),
                                        ),
                                      ],
                                    ),
                                  )
                                ],
                              )));
                    },
                  );
                }).toList(),
              ),
            ));
          }
        } else {
          return Container(
            margin: EdgeInsets.fromLTRB(200, 300, 150, 100),
            child: CircularProgressIndicator(),
          );
        }
      },
    );
  }

  Widget _showDialog(
      BuildContext context, String title, String content, Color titleColor) {
    showDialog(
        context: context,
        barrierDismissible: true,
        builder: (context) {
          return AlertDialog(
            content: Container(
              height: MediaQuery.of(context).size.height * .4,
              decoration: BoxDecoration(
                color: anCream,
                border: Border.all(color: Colors.black, width: 2.0),
                borderRadius: BorderRadius.circular(10.0),
              ),
              child: Column(
                children: <Widget>[
                  Container(
                      margin: EdgeInsets.only(top: 20.0),
                      child: Text(
                        title,
                        style: TextStyle(
                            color: titleColor, fontWeight: FontWeight.bold),
                      )),
                  Container(
                    margin: EdgeInsets.only(top: 5.0, left: 20.0, right: 20.0),
                    child: Divider(
                      height: 15,
                      color: Colors.black,
                    ),
                  ),
                  Container(
                    height: MediaQuery.of(context).size.height * .28,
                    width: double.maxFinite,
                    margin: EdgeInsets.only(top: 10.0, right: 20.0, left: 20.0),
                    child: ListView(
                      shrinkWrap: true,
                      children: <Widget>[
                        Text(
                          content,
                          style: TextStyle(
                              fontWeight: FontWeight.bold, fontSize: 12.0),
                        )
                      ],
                    ),
                  )
                ],
              ),
            ),
          );
        });
  }
}
