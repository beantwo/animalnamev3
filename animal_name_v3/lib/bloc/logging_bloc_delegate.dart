import 'package:bloc/bloc.dart';

class LoggingBlocDelegate extends BlocDelegate {

  @override
  void onTransition(Transition transition) {
    print(transition);
  }

  @override
  void onError(Object error, StackTrace stacktrace) {
    print('[BlocError] $error, $stacktrace');
  }
}
