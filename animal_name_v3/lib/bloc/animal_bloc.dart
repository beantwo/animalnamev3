import 'package:bloc/bloc.dart';
import '../services/animal_service.dart';
import '../objects/animal_object.dart';

class AnimalState {
  final bool isError;
  final bool isFinished;
  final int length;
  String tag;
  List<Animal> animals = new List<Animal>();

  AnimalState({this.isError = false, this.isFinished = false, this.length = 0, this.animals, this.tag = ''});

  @override
  String toString() {
    return 'AnimalState{isError: $isError, '
        'isFinished: $isFinished, '
        'tag: $length '
        '}';
  }

}

class AnimalBloc extends Bloc<int, AnimalState> {

  @override
  AnimalState get initialState {
    return AnimalState(isError: false, isFinished: false);
  }

  @override
  Stream<AnimalState> mapEventToState(AnimalState currentState, int event) async* {
    try {
      currentState.animals = await AnimalService().getAnimalsByTag(currentState.tag);
      yield AnimalState(isError: false, isFinished: true, length: currentState.animals.length, animals: currentState.animals);
    } catch (e) {
      print('Error: $e');
      yield AnimalState(isError: true);
    }
  }
}