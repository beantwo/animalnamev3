import 'package:bloc/bloc.dart';
import '../services/animal_service.dart';

class InitState {
  final bool isError;
  final int prevValue;
  final int currentValue;

  InitState(this.isError, this.prevValue, this.currentValue);

  bool isFinished() => (currentValue == 100);

  @override
  String toString() {
    return 'InitState{isError: $isError, '
        'prevValue: $prevValue, '
        'currentValue: $currentValue'
        '}';
  }

}

class InitBloc extends Bloc<int, InitState> {

  @override
  InitState get initialState {
    return InitState(false, 0, 0);
  }

  @override
  Stream<InitState> mapEventToState(InitState currentState, int event) async* {
    int prev = currentState.currentValue; int current = 10;
    yield InitState(false, prev, current);
    try {
      await AnimalService().generateAnimalGroups();
      prev = current; current = 100;
      yield InitState(false, prev, current);
    } catch (e) {
      print('Error: $e');
      yield InitState(true, prev, current);
    }
  }
}